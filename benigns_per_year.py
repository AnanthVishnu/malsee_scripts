import pandas as pd 
import matplotlib.pyplot as plt

df = pd.read_csv('latest.csv')
vt_score = df["vt_detection"]
dex_dates = [i.split("-")[0] for i in df["dex_date"]]
benign_count = {}
for i in range(len(dex_dates)):
    if dex_dates[i] in benign_count:
        if(vt_score[i]==0.0):
            benign_count[dex_dates[i]]+=1
    else:
        if vt_score[i] == 0.0:
            benign_count[dex_dates[i]] = 1
        else:
            pass

benign_count_per_year_df = pd.DataFrame(benign_count.items())
benign_count_per_year_df.columns = ["Year", "count"]
benign_count_per_year_df = benign_count_per_year_df.sort_values(by = 'count', ascending = False)
benign_count_per_year_df[['Year', 'count']].plot(kind='bar', x='Year', y='count',  logy=True, grid=True, title="Benign distribution", xlabel="dex_date", ylabel="number of benign samples", figsize=(50,20), legend=False)
plt.savefig("benign_distribution.png")