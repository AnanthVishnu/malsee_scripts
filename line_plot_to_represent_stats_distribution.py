import matplotlib.pyplot as plt
from numpy.core.fromnumeric import sort
import pandas as pd
import sys
import numpy as np
import itertools

df = pd.read_excel('Malsee_classification_experiments.xlsx',engine='openpyxl')
col = df[sys.argv[1]][20:36]
vals = [float(i.split("±")[0]) for i in col]

model_set = ["RF", "KNN-0", "KNN-1", "KNN-2"]
feature_set = ['Image', 'Audio', 'Hash', 'Concatenated']
d = {}

d["Model"] = list(itertools.chain.from_iterable(itertools.repeat(x, len(feature_set)) for x in model_set))
d["Feature"] = feature_set*len(feature_set)
d["eval_metrcis"] = vals

df = pd.DataFrame(data=d)
color_dict = {"RF": 'skyblue', "KNN-0": 'red', "KNN-1": 'green', "KNN-2": 'orange'}

plt.clf()
for model in model_set:
     selected_data = df.loc[df['Model'] == model]
     val = list(selected_data["Model"])
     plt.plot(selected_data['Feature'], selected_data['eval_metrcis'], c = color_dict[model], label=model)

xs = np.array(df["Feature"])
ys = np.array(df["eval_metrcis"])

for x,y in zip(xs,ys):
     print(y)
     plt.annotate(y, 
     (x,y),
     textcoords="offset points",
     xytext=(0,4),
     fontsize = 6,
     ha='center')

plt.legend()
plt.grid()
plt.xlabel("Features")
plt.ylabel(sys.argv[1])
plt.title(f'Distribution_of_{sys.argv[1]}_scores_with_equalsplit_distribution_test')
plt.savefig(f'Distribution_of_{sys.argv[1]}_scores_with_equalsplit_distribution_test.png')