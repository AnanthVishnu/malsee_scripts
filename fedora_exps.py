from genericpath import exists
from get_byteplot_image import *
from get_bigram_dct_image import *
from PIL import Image
import os
import cv2

OS = "Fedora"
list = ["bash", "cat", "cp", "dir", "ln", "ls", "mkdir", "sleep", "tar", "true"]

list32 = []
list33 = []
list34 = []

p32 = "../fedora_32_(Workstation_Edition)_2020-04-28/bin"
p33 = "../fedora_33_(Workstation_Edition)_2020-10-27/bin"
p34 = "../fedora_34_(Workstation_Edition)_2021-04-27/bin"

for root, dirs, files in os.walk(p32):
    list32.extend(files)
for root, dirs, files in os.walk(p33):
    list33.extend(files)
for root, dirs, files in os.walk(p34):
    list34.extend(files)

print("done")

for  i in list:
    if not os.path.exists(i):
        os.mkdir(i)
    
    os.makedirs(os.path.join(i, OS+"32"), exist_ok=True)
    os.makedirs(os.path.join(i, OS+"33"), exist_ok=True)
    os.makedirs(os.path.join(i, OS+"34"), exist_ok=True)
    
    name = f'{i}_{OS}32_byteplot.png'
    bpi32 = get_byteplot_image(os.path.join(p32,i))
    im_bpi32 = Image.fromarray(bpi32)
    im_bpi32.save(os.path.join(i, OS+"32",name))
    
    name = f'{i}_{OS}32_bigram.png'
    b32 = (get_bigram_dct_image(os.path.join(p32,i))[0]*255).astype(np.uint8)
    im_b32 = Image.fromarray(b32, mode='L')
    im_b32.save(os.path.join(i, OS+"32",name))
    
    name = f'{i}_{OS}32_bigram_dct.png'
    bdct32 = get_bigram_dct_image(os.path.join(p32,i))[1]
    im_bdct32 = Image.fromarray(bdct32)
    im_bdct32.save(os.path.join(i, OS+"32",name))

    name = f'{i}_{OS}33_byteplot.png'
    bpi33 = get_byteplot_image(os.path.join(p33,i))
    im_bpi33 = Image.fromarray(bpi33)
    im_bpi33.save(os.path.join(i, OS+"33",name))
    
    name = f'{i}_{OS}33_bigram.png'
    b33 = (get_bigram_dct_image(os.path.join(p33,i))[0]*255).astype('uint8')
    im_b33 = Image.fromarray(b33, mode='L')
    im_b33.save(os.path.join(i, OS+"33",name))
    
    name = f'{i}_{OS}33_bigram_dct.png'
    bdct33 = get_bigram_dct_image(os.path.join(p33,i))[1]
    im_bdct33 = Image.fromarray(bdct33)
    im_bdct33.save(os.path.join(i, OS+"33",name))

    name = f'{i}_{OS}34_byteplot.png'
    bpi34 = get_byteplot_image(os.path.join(p34,i))
    im_bpi34 = Image.fromarray(bpi34)
    im_bpi34.save(os.path.join(i, OS+"34",name))
    
    name = f'{i}_{OS}34_bigram.png'
    b34 = (get_bigram_dct_image(os.path.join(p34,i))[0]*255).astype('uint8')
    im_b34 = Image.fromarray(b34, mode='L')
    im_b34.save(os.path.join(i, OS+"34",name))
    
    name = f'{i}_{OS}34_bigram_dct.png'
    bdct34 = get_bigram_dct_image(os.path.join(p34,i))[1]
    im_bdct34 = Image.fromarray(bdct34)
    im_bdct34.save(os.path.join(i, OS+"34",name))
