#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  7 14:24:03 2017

@author: manhar
"""
import os, sys, glob
import numpy as np
from PIL import Image
import array
import hashlib
from joblib import Parallel, delayed
from tqdm import tqdm
import argparse
NUM_CORES = 5

parser = argparse.ArgumentParser()
parser.add_argument(
    "-b", "--bindir",
    type=str,
    required=True,
    help="Path to input directory with binaries"
)

parser.add_argument(
    "-o", "--outdir",
    type=str,
    required=True,
    help="Path to output directory"
)

parser.add_argument(
    "-w", "--imgwidth",
    type=int,
    default=-1,
    required=False,
    help="Image width"
)

args = vars(parser.parse_args())

bin_dir = args["bindir"]# '~/small-data/benign/bins/' (fullpath)    
out_loc = args["outdir"] # '~/images/' (fullpath)

bin_files = [os.path.join(dp, f) for dp, dn, fn in os.walk(bin_dir) for f in fn]    
    
cnt = len(bin_files)

def extract_img(i):
    file_loc = bin_files[i]
    filename = os.path.basename(file_loc)
    sha256 = hashlib.sha256(open(file_loc, 'rb').read()).hexdigest() 
    out1 = os.path.join(sha256[0],sha256[1], sha256[2], sha256[3],sha256[4])
    out2 = os.path.join(out_loc, str(args["imgwidth"]), out1)    
    if not os.path.exists(out2):
        os.makedirs(out2)       
    out = os.path.join(out2, sha256 + "_byteplotimg_" + str(args["imgwidth"]) + ".png")
    if not os.path.isfile(out):
        ln = os.path.getsize(file_loc)
        file_size = ln/1024
        f = open(file_loc,'rb')
        if file_size < 10:
            col_size = 32
        elif file_size >= 10 and file_size < 30:
            col_size = 64
        elif file_size >= 30 and file_size < 60:
            col_size = 128
        elif file_size >= 60 and file_size < 100:
            col_size = 256
        elif file_size >= 100 and file_size < 200:
            col_size = 384
        elif file_size >= 200 and file_size < 500:
            col_size = 512
        elif file_size >= 500 and file_size < 1000:
            col_size = 768
        else: # file_size >= 1000:
            col_size = 1024
        rem = ln%col_size # remainder of length of file in bytes and col_size
        if ln == rem:
            assert ln < 1024, str(ln)
            rem = 0
        a = array.array("B") # uint8 array
        a.fromfile(f,ln-rem) # so that ln-rem is a multiple of col_size
        f.close()
        if ln < col_size:
            g = np.expand_dims(np.pad(a, ((0,col_size-ln),), mode='constant'), 0)
            print("g.shape "+str(g.shape))
        else:
            g = np.reshape(a,(-1,col_size)) # reshap vector to matrix with 'col_size' cols. reshaping done rowwise
        g = np.uint8(g)    
        im = Image.fromarray(g)
        
        if(args["imgwidth"]==-1):
            im.save(out)
        else:
            im_resized = im.resize((int(args["imgwidth"]), int(args["imgwidth"])))
            im_resized.save(out)
    return None    
 
feat_mat1 = Parallel(n_jobs=NUM_CORES)(delayed(extract_img)(i) for i in tqdm(range(cnt)))

    
