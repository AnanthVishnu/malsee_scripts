import argparse
import h5py
import numpy as np 
import h5py
import os
import sys
import numpy as np
from sklearn import metrics
from tqdm import tqdm
import warnings
import time
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.utils import shuffle
import matplotlib
matplotlib.use('AGG')
import matplotlib.pyplot as plt
from sklearn.metrics import plot_confusion_matrix
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix
import sklearn.neighbors
from mc_knn import *
import numpy as np
import pickle
import statistics
import time
warnings.filterwarnings("ignore")
os.chdir(os.path.dirname(os.path.realpath(__file__)))

def generate_model(modelname, ncores, X, y):
    m = "euclidean"
    if(modelname=="RF-0"):
        model = RandomForestClassifier(n_jobs=ncores)
        model.fit(X, y)
    elif(modelname=="RF-1"):
        model = RandomForestClassifier(n_jobs=ncores, class_weight="balanced")
        model.fit(X, y)
    elif(modelname=="KNN-0"):
        model = KNeighborsClassifier(n_jobs=ncores)
        model.fit(X, y)
    elif(modelname == "KNN-1"):
        model = Annoy(m, 100, -1, ncores)
        model.fit(X)
    elif(modelname == "KNN-2"):
        model = Annoy(m, 100, 10000, ncores)
        model.fit(X)
    elif(modelname == "KNN-3"):
        model = KNeighborsClassifier(n_jobs=ncores, weights="distance")
        model.fit(X, y)
    elif(modelname == "lshf"):
        model = LSHF(m, 20, 100)
        model.fit(X)
    elif(modelname == "ball"):
        model = BallTree(m, 100)
        model.fit(X)
    elif(modelname == "kd"):
        model = KDTree(m, 1000)
        model.fit(X)
    elif(modelname == "bf-blas"):
        model = BruteForceBLAS(m)
        model.fit(X)
    elif(modelname == "bf"):
        model = BruteForce(m)
        model.fit(X)
    return model

def generate_predictions(modelname, label_array, model, X): 
    if(modelname=="RF-0" or modelname=="KNN-0" or modelname=="RF-1" or modelname=="KNN-3"):
        y_pred = model.predict(X)
    
    elif(modelname=="lshf" or modelname=="ball" or modelname=="kd"or modelname=="bf-blas" or modelname=="bf"):
        nn_indices = model.query(X, 5)
        arr = np.take(label_array,np.take(nn_indices, np.arange(nn_indices.shape[0]), axis=0))
        y_pred = []
        for x in arr:
            if(np.count_nonzero(x==1)>np.count_nonzero(x==0)):
                y_pred.append(1)
            else:
                y_pred.append(0)
        
    elif(modelname == "KNN-1" or modelname == "KNN-2"):
        for X in X_test:
            kc_nbrs_indices = np.array(model.query(X, 5))
            nn_labels = np.take(label_array, kc_nbrs_indices)
            if(np.count_nonzero(nn_labels == 1)>np.count_nonzero(nn_labels == 0)):
                y_pred = 1
            elif(np.count_nonzero(nn_labels == 1)<np.count_nonzero(nn_labels == 0)):
                y_pred = 0
    return y_pred

def calculate_metrics(y_true, y_pred):
    acc = accuracy_score(y_true, y_pred)
    bal_acc = balanced_accuracy_score(y_true, y_pred)
    prec_macro = precision_score(y_true, y_pred, average="macro")
    prec_wtd = precision_score(y_true, y_pred, average="weighted")
    rec_macro = recall_score(y_true, y_pred, average="macro")
    rec_wtd = recall_score(y_true, y_pred, average="weighted")
    f1_macro = f1_score(y_true, y_pred, average="macro")
    f1_wtd = f1_score(y_true, y_pred, average="weighted")
    cm = confusion_matrix(y_true, y_pred, normalize="true")
    return [acc ,bal_acc, prec_macro, prec_wtd, rec_macro, rec_wtd, f1_macro, f1_wtd, cm]



parser = argparse.ArgumentParser()
parser.add_argument(
    "-h5", "--hdf5",
    type=str,
    required=True,
    help="HDF5 with malware and benign samples"
)

parser.add_argument(
    "-f", "--feat",
    type=str,
    required=True,
    help="feature vector to use"
)

parser.add_argument(
    "-r", "--ratio",
    type=float,
    default="None",
    required=True,
    help="ratio of malware:benign samples"
)

parser.add_argument(
    "-m", "--model",
    type=str,
    required=False,
    default=None,
    help="Scikit-Learn model to use"
)

parser.add_argument(
    "-kf", "--kfold",
    type=int,
    required=False,
    default=10,
    help="Number of folds"
)

parser.add_argument(
    "-rseed", "--seed",
    type=int,
    required=False,
    default=7,
    help="Random seed"
)

parser.add_argument(
    "-ncores", "--cores",
    type=int,
    required=False,
    default=90,
    help="Number of cores to use"
)

parser.add_argument(
    "-o", "--out",
    type=str,
    required=False,
    help="output directory"
)

args = vars(parser.parse_args())

f = h5py.File(args["hdf5"], "r")

mal_ind = np.where(np.array(f['labels'])==1)[0]
ben_ind = np.where(np.array(f['labels'])==0)[0]

total_mal = mal_ind.shape[0]
total_ben = ben_ind.shape[0]

if(args["ratio"]== None):
    print("Number of Malware samples chosen:", total_mal)
    print("Number of Benign samples chosen:", total_ben)
    if(args["feat"]=="concatenated"):
        image_feats = f["feats_image"]
        audio_feats = f["feats_audio"]
        hash_feats = f["feats_hash"]
        feat_array = np.concatenate((image_feats,audio_feats,hash_feats), axis=1)
    else:
        feat_array = f[args["feat"]]
    label_array = f["labels"]

else:
    mal_count_required = int(args["ratio"]*total_ben)
    print("Number of Malware samples chosen:", mal_count_required)
    print("Number of Benign samples chosen:", total_ben)

    np.random.shuffle(mal_ind)
    x=np.random.choice(mal_ind, size=mal_count_required)

    mal_ben_ind = np.concatenate((ben_ind, x))
    np.random.shuffle(mal_ben_ind)
    label_array = np.take(f['labels'], mal_ben_ind)

    if(args["feat"]=="concatenated"):
        image_feats = np.take(f['feats_image'], mal_ben_ind, axis=0)
        audio_feats = np.take(f['feats_audio'], mal_ben_ind, axis=0)
        hash_feats = np.take(f['feats_hash'], mal_ben_ind, axis=0)
        feat_array = np.concatenate((image_feats,audio_feats,hash_feats), axis=1)

    else:
        feat_array = np.take(f[args['feat']], mal_ben_ind, axis=0)
        
if(args["out"] == None):
    dir_name = str(mal_count_required)+"_"+str(total_ben)+"_"+args["model"]+"_"+args["feat"]
    print("dir_name:", dir_name)
    if not (os.path.isdir(dir_name) and os.path.exists(dir_name)):
        os.makedirs(dir_name)
    log_file = os.path.join(dir_name+".log")
    print("log file:", log_file)
else:
    if not (os.path.isdir(args["out"]) and os.path.exists(args["out"])):
        os.makedirs(args["out"])
    log_file = os.path.join(args["out"]+".log")

assert args["feat"] in [
    "feats_image",
    "feats_audio",
    "feats_hash",
    "concatenated",
    "avast_scores",
    "bitdefender_scores",
], "<feat> argument invalid"

assert args["model"] in [
    "KNN-0",
    "KNN-1",
    "KNN-2",
    "KNN-3",
    "RF-0",
    "RF-1",
    "lshf",
    "ball",
    "kd",
    "blas",
    "bf-blas",
    None,
], "<model> argument invalid"

seed = args["seed"]
skf = StratifiedKFold(n_splits=args["kfold"], random_state=seed, shuffle=True)
hist_flag = True
model = args["model"]
NUM_CORES = args["cores"]

with open(log_file, "w") as file:
    msg = (
        "********************************************************\n"
        + "Feature: {}\n".format(args["feat"])
        + "********************************************************"
    )
    file.write(msg + "\n")
    print(msg)
    X = feat_array
    print("Shape of fetaure vector:", X.shape)
    y = label_array
    print("Number of labels:", len(y))

    acc_list = []
    bal_acc_list = []
    prec_macro = []
    prec_weighted = []
    recall_macro = []
    recall_weighted = []
    f1_macro = []
    f1_weighted = []
    cm_list = []
    
    hist_flag = True
    
    if(args["feat"]=="avast_scores" or args["feat"] == "bitdefender_scores"):
        y_pred = np.take(f[args["feat"]], mal_ben_ind)
        eval_metrics = calculate_metrics(y, y_pred)
        msg = (
        f"Accuracy-{eval_metrics[0]}"
        f"Balanced Accuracy-{eval_metrics[1]}\n"
        f"Macro Precision-{eval_metrics[2]}\n"
        f"Weighted Precision-{eval_metrics[3]}\n"
        f"Macro Recall-{eval+metrics[4]}\n"
        f"Weighted Recall-{eval_metrics[5]}\n"
        f"Macro F1-{eval_metrics[6]}\n"
        f"weighted F1-{eval_metrics[7]}\n"
        f"Normalized CM-{eval_metrics[8]}\n")
        print(msg)
        print(eval_metrics[8])
        file.write(msg)
        file.write("\n")
    
    else:
        start = time.time()
        with tqdm(total=10) as progress_bar:
            for train_index, test_index in skf.split(X, y):
                X_train, X_test = X[train_index], X[test_index]
                y_train, y_test = y[train_index], y[test_index]
                if hist_flag:
                    df_train = pd.DataFrame(y_train, columns=["Train"])
                    df_test = pd.DataFrame(y_test, columns=["Test"])
                    df = pd.concat([df_train, df_test], ignore_index=True, axis=1)
                    df.columns = ["Train", "Test"]
                    df.apply(
                        lambda s: s.value_counts(sort=True).sort_index(ascending=True)
                    ).plot.bar()
                    plt.grid(linestyle="dotted")
                    plt.title("Histogram of train-test split")
                    plt.tight_layout()
                    plt.xlabel("Label")
                    plt.ylabel("Number of samples")
                    save_loc = os.path.join(dir_name, "histogram_train_test_split_" + str(args["kfold"]) + ".png")
                    plt.savefig(save_loc, bbox_inches="tight")
                hist_flag = False
                
                clf = generate_model(args["model"], NUM_CORES, X_train, y_train)
            
                y_pred = generate_predictions(args["model"], label_array, clf, X_test)
                eval_metrics = calculate_metrics(y_test, y_pred)
                acc_list.append(eval_metrics[0])
                bal_acc_list.append(eval_metrics[1])
                prec_macro.append(eval_metrics[2])
                prec_weighted.append(eval_metrics[3])
                recall_macro.append(eval_metrics[4])
                recall_weighted.append(eval_metrics[5])
                f1_macro.append(eval_metrics[6])
                f1_weighted.append(eval_metrics[7])
                cm_list.append(eval_metrics[8])
                progress_bar.update(1)
        
        msg = (
        f"Accuracy-{statistics.mean(acc_list)}({statistics.stdev(acc_list)})\n"
        f"Balanced Accuracy-{statistics.mean(bal_acc_list)}({statistics.stdev(bal_acc_list)})\n"
        f"Macro Precision-{statistics.mean(prec_macro)}({statistics.stdev(prec_macro)})\n"
        f"Weighted Precision-{statistics.mean(prec_weighted)}({statistics.stdev(prec_weighted)})\n"
        f"Macro Recall-{statistics.mean(recall_macro)}({statistics.stdev(recall_macro)})\n"
        f"Weighted Recall-{statistics.mean(recall_weighted)}({statistics.stdev(recall_weighted)})\n"
        f"Macro F1-{statistics.mean(f1_macro)}({statistics.stdev(f1_macro)}),\n"
        f"weighted F1-{statistics.mean(f1_weighted)}({statistics.stdev(f1_weighted)})\n"
        f"Normalized CM-{np.mean(np.array(cm_list), axis=0)}({np.std(np.array(cm_list), axis=0)})\n")
        
        file.write(msg)
        file.write("\n")
        print(msg)
        print(np.mean(np.array(cm_list), axis=0))
        end = time.time()
        print("Time:", end-start)






