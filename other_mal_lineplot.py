import sys
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np 
import h5py
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import NearestNeighbors
import matplotlib.pyplot as plt
import pandas
import itertools
import sys

f=h5py.File("9.03M_samples.hdf5",'r')
la=np.array(f["avast_scores"])
lb=np.array(f["bitdefender_scores"])
vtpos = np.where(np.array(f["vt_positives"])>0)[0]
ben=np.where(np.array(f["vt_positives"])==0)[0]

az = np.intersect1d(np.where(la==0)[0], vtpos)
bz = np.intersect1d(np.where(lb==0)[0], vtpos)
a1 = np.intersect1d(np.where(la==1)[0], vtpos)
b1 = np.intersect1d(np.where(lb==1)[0], vtpos)

azbz=np.intersect1d(az,bz)
a1b1=np.intersect1d(a1,b1)
a1bz=np.intersect1d(a1,bz)
azb1=np.intersect1d(az,b1)
print(azbz.shape, a1b1.shape, a1bz.shape, azb1.shape)
feats_image_azbz = np.take(f['feats_image'],azbz, axis=0)
feats_image_a1b1 = np.take(f['feats_image'],a1b1, axis=0)
feats_image_a1bz = np.take(f['feats_image'],a1bz, axis=0)
feats_image_azb1 = np.take(f['feats_image'],azb1, axis=0)
feats_image_benign = np.take(f['feats_image'], ben, axis=0)

labels_a1b1 = np.take(f['labels'],a1b1)
labels_a1bz = np.take(f['labels'],a1bz)
labels_azb1 = np.take(f['labels'],azb1)
labels_benign = np.take(f['labels'],ben)

hash_azbz = np.take(f['md5s'],azbz)
hash_a1b1 = np.take(f['md5s'],a1b1)
hash_a1bz = np.take(f['md5s'],a1bz)
hash_azb1 = np.take(f['md5s'],azb1)
hash_benign = np.take(f['md5s'],ben)

vtpos_azbz = np.take(f['vt_positives'],azbz)
vtpos_a1b1 = np.take(f['vt_positives'],a1b1)
vtpos_a1bz = np.take(f['vt_positives'],a1bz)
vtpos_azb1 = np.take(f['vt_positives'],azb1)
vtpos_benign = np.take(f['vt_positives'],ben)

vttot_azbz = np.take(f['vt_totals'],azbz)
vttot_a1b1 = np.take(f['vt_totals'],a1b1)
vttot_a1bz = np.take(f['vt_totals'],a1bz)
vttot_azb1 = np.take(f['vt_totals'],azb1)
vtot_benign = np.take(f['vt_totals'],ben)

neigh_a1b1 = NearestNeighbors(n_neighbors=1)
neigh_a1b1.fit(feats_image_a1b1)
neigh_a1bz = NearestNeighbors(n_neighbors=1)
neigh_a1bz.fit(feats_image_a1bz)
neigh_azb1 = NearestNeighbors(n_neighbors=1)
neigh_azb1.fit(feats_image_azb1)
neigh_benign = NearestNeighbors(n_neighbors=1)
neigh_benign.fit(feats_image_benign)
dist_a1b1 = []
dist_a1bz = []
dist_azb1 = []
dist_benign = []
no_of_samples = sys.argv[1]

with open("info.txt", "w") as file:
    dict = {}
    dict["query_md5"] = []
    dict["query_vtpos"] = []
    dict["query_vtotals"] = []
    dict["a1b1neigh_md5"] = []
    dict["a1b1neigh_vtpos"] = []
    dict["a1b1neigh_vtotals"] = []
    dict["a1b1neigh_dist"] = []
    dict["a1bzneigh_md5"] = []
    dict["a1bzneigh_tpos"] = []
    dict["a1bzneigh_vtotals"] = []
    dict["a1bzneigh_dist"] = []
    dict["azb1neigh_md5"] = []
    dict["azb1neigh_vtpos"] = []
    dict["azb1neigh_vtotals"] = []
    dict["azb1neigh_dist"] = []
    dict["benign_md5"] = []
    dict["benign_vtpos"] = []
    dict["benign_vtotals"] = []
    dict["benign_dist"] = []

    for i in range(int(no_of_samples)):
        x = feats_image_azbz[i]
        a1b1_info = neigh_a1b1.kneighbors(x.reshape(1,-1))
        a1bz_info = neigh_a1bz.kneighbors(x.reshape(1,-1))
        azb1_info = neigh_azb1.kneighbors(x.reshape(1,-1))
        benign_info = neigh_benign.kneighbors(x.reshape(1,-1))
        
        dist_a1b1.append(a1b1_info[0][0][0])
        dist_a1bz.append(a1bz_info[0][0][0])
        dist_azb1.append(azb1_info[0][0][0])
        dist_benign.append(benign_info[0][0][0])
        
        azbz_idx = i
        a1b1_idx = a1b1_info[1][0][0]
        a1bz_idx = a1bz_info[1][0][0]
        azb1_idx = azb1_info[1][0][0]
        benign_idx = benign_info[1][0][0]

        dict["query_md5"].append(hash_azbz[azbz_idx])
        dict["query_vtpos"].append(vtpos_azbz[azbz_idx])
        dict["query_vtotals"].append(vttot_azbz[azbz_idx])

        dict["a1bzneigh_md5"].append(hash_a1bz[a1bz_idx])
        dict["a1bzneigh_tpos"].append(vtpos_a1bz[a1bz_idx])
        dict["a1bzneigh_vtotals"].append(vttot_a1bz[a1bz_idx])
        dict["a1bzneigh_dist"].append(str(a1bz_info[0][0][0]))

        dict["azb1neigh_md5"].append(hash_azb1[azb1_idx])
        dict["azb1neigh_vtpos"].append(vtpos_azb1[azb1_idx])
        dict["azb1neigh_vtotals"].append(vttot_azb1[azb1_idx])
        dict["azb1neigh_dist"].append(str(azb1_info[0][0][0]))

        dict["a1b1neigh_md5"].append(hash_a1b1[a1b1_idx])
        dict["a1b1neigh_vtpos"].append(vtpos_a1b1[a1b1_idx])
        dict["a1b1neigh_vtotals"].append(vttot_a1b1[a1b1_idx])
        dict["a1b1neigh_dist"].append(str(a1b1_info[0][0][0]))

        dict["benign_md5"].append(hash_benign[benign_idx])
        dict["benign_vtpos"].append(vtpos_benign[benign_idx])
        dict["benign_vtotals"].append(vtot_benign[benign_idx])
        dict["benign_dist"].append(str(benign_info[0][0][0]))

    df = pandas.DataFrame(data=dict)
    df = df.reset_index()
    df.to_csv("info.csv", sep='\t')

labels = ["avast", "bf", "abf", "benign"]

color_dict = {"avast": 'magenta', "bf": 'sienna', "abf": 'crimson', "benign": 'green'}

d= {}

d["label"] = list(itertools.chain.from_iterable(itertools.repeat(x, int(no_of_samples)) for x in labels))
list_new = ["S"+str(i) for i in range(int(no_of_samples))] 
d["sample"] = list_new*len(labels)
d["distance"] = list(itertools.chain(dist_a1bz, dist_azb1, dist_a1b1, dist_benign))

df = pandas.DataFrame(data=d)
print(df)
label_set = set(df['label'])
plt.figure()

plt.clf()
for label in label_set:
     selected_data = df.loc[df['label'] == label]
     plt.plot(selected_data['sample'], selected_data['distance'], c = color_dict[label], label=label)

plt.grid()
plt.legend()
plt.xlabel("Samples from Avast-0 Bitdef-0 bin")
plt.ylabel("NN distance ")
plt.savefig("labels_new.png")
